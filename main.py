import pyxhook
import pyautogui
import os
from time import sleep

def kbevent(e):
  print(e.Key)
  if e.Key=="T":
    os.system('lxterminal -e bash input.sh')
    sleep(.5)
    pyautogui.hotkey('ctrl','v')
    pyautogui.typewrite(['\n'])

hookman = pyxhook.HookManager()
hookman.KeyDown = kbevent
hookman.HookKeyboard()
hookman.start()
